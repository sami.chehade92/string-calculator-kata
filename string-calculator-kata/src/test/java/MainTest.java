import org.junit.jupiter.api.Test;

import java.util.stream.IntStream;

import static java.util.stream.Collectors.joining;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class MainTest {

    @Test
    public void add_emptyString() {
        int res = Main.add("");
        assertEquals(0, res);
    }

    @Test
    public void add_1number() {
        int res = Main.add("1");
        assertEquals(1, res);
    }

    @Test
    public void add_2numbers() {
        int res = Main.add("1,2");
        assertEquals(3, res);
    }

    @Test
    public void add_manyNumbers() {
        var numbers = IntStream.iterate(1, n -> n+1).limit(20)
                .boxed().map(Object::toString).collect(joining(","));
        assertEquals(210, Main.add(numbers));
    }

    @Test
    public void add_withCommaAndSpace() {
        assertEquals(6, Main.add("1\n2,3"));
    }

    @Test
    public void add_withDifferentDelimiters() {
        assertEquals(6, Main.add("//;\n1\n2,3"));
    }

    @Test
    public void add_withNegativeNumbers() {
        assertThrows(IllegalArgumentException.class, () -> Main.add("//;\n1\n2,3,-5,3,21,45,-32"));
        try {
            Main.add("//;\n1\n2,3,-5,3,21,45,-32");
        } catch (IllegalArgumentException e) {
            assertEquals("negatives not allowed. You've used the following negative numbers: [-5, -32]", e.getMessage());
        }
    }

    @Test
    public void add_incorrectFormat() {
        assertThrows(NumberFormatException.class, () -> Main.add("aa"));
    }
}
