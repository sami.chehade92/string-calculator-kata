import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static int add(String numbers) {
        if ("".equals(numbers)) {
            return 0;
        }
        String delimiterRegex = "[,\n";
        List<Integer> negativeNumbers = new ArrayList<>();
        if (numbers.startsWith("//")) {
            delimiterRegex += numbers.charAt(2);
            numbers = numbers.substring(4);
        }
        delimiterRegex += "]";
        int sum = Arrays.stream(numbers.split(delimiterRegex))
                .mapToInt(Integer::parseInt)
                .peek(i -> {
                    if (i < 0) {
                        negativeNumbers.add(i);
                    }
                })
                .sum();
        if (negativeNumbers.size() > 0) {
            throw new IllegalArgumentException("negatives not allowed. You've used the following negative numbers: " + negativeNumbers);
        }
        return sum;
    }
}
