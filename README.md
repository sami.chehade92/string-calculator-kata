# String Calculator Kata - by Sami Chehade

## The necessary tools

We list here the tools that you need to run this project. Between parentheses is the version that we personaly use to run it.

- Java (11.0.13)
- Maven (3.6.3)

## Run the tests

To test the project, you can run the tests like so :

```
cd string-calculator-kata
mvn test
```
